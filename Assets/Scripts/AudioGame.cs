﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioGame : MonoBehaviour
{
    private void Start()
    {
        var audio = GetComponent<AudioSys>();
        audio.AudioTheme.Stop();
        audio.AudioGame.Play();
    }
}
