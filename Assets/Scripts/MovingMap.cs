﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingMap : MonoBehaviour
{
    [SerializeField]
    private float m_ScrollSpeed = 0.5f;
    public float ScrollSpeed
    {
        get { return m_ScrollSpeed; }
        set { m_ScrollSpeed = value; }
    }

    [SerializeField]
    private Renderer[] m_Rend;

    private void Update()
    {
        float offset = Time.time * m_ScrollSpeed;
        foreach (Renderer rend in m_Rend)
        {
            rend.material.mainTextureOffset = new Vector2(offset, 0);
        }
    }
}
