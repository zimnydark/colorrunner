﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScene : MonoBehaviour
{
    private void Start()
    {
        var audio = GetComponent<AudioSys>();
        audio.AudioTheme.Play();
        audio.AudioGame.Stop();
    }

}
