﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MoveFunc : MonoBehaviour, IPointerDownHandler
{
    public UnityEvent Move;
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        Move.Invoke();
    }
}
