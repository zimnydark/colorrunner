﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBehaviour : MonoBehaviour
{
    private Manager Manager;
    private float m_Speed = 8f;

    private Vector3 m_Direction = new Vector3(1, 0, 0);

    private void Start()
    {
        Manager = GameObject.Find("GameManager").GetComponent<Manager>();
        m_Speed = Manager.ObstacleSpeed;
    }
    private void Update()
    {
        m_Speed = Manager.ObstacleSpeed;
        transform.Translate(m_Direction * m_Speed * Time.deltaTime);
    }
}
