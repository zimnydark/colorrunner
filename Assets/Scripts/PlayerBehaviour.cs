﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public static int FloorNumber = 0;

    public Manager Manager;

    public float SpeedMove = 1f;

    private bool m_Dead = false;
    public bool IsDead
    {
        get { return m_Dead; }
        set { m_Dead = value; }
    }

    private Transform NewPos;
    [SerializeField]
    private GameObject m_Holder;
    [SerializeField]
    private GameObject m_Character;

    private void Start()
    {
        FloorNumber = 0;
        m_Holder.transform.position = transform.position;
        m_Holder.transform.rotation = transform.rotation;
        NewPos = m_Holder.transform.GetChild(0);
        NewPos.position = transform.GetChild(0).position;
        NewPos.rotation = transform.GetChild(0).rotation;
    }
    private void Update()
    {
        //For tests on windows
        if(Input.GetKeyUp(KeyCode.A) && !IsDead)
        {
            m_Holder.transform.Rotate(36, 0, 0);
            FloorNumber++;
        }
        else if(Input.GetKeyUp(KeyCode.D) && !IsDead)
        {
            m_Holder.transform.Rotate(-36, 0, 0);
            FloorNumber--;
        }
        //
        UpdateFloorNumer();
        MovePlayer(m_Holder.transform, Vector3.Distance(NewPos.position, transform.GetChild(0).position));
        ChangeColor();
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.GetComponent<Renderer>().material.color == m_Character.GetComponent<Renderer>().material.color)
        {
            //do something when the color is the same
        }
        else
        {
            Die();
        }
    }

    private void MovePlayer(Transform newPost,float speed)
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, newPost.rotation, SpeedMove * Mathf.Sqrt(speed) * Time.deltaTime);
    }
    private void ChangeColor()
    {
        m_Character.GetComponent<Renderer>().material.color = Manager.GetPlayersPlatformColor();
    }
    private void UpdateFloorNumer()
    {
        if (FloorNumber < 0)
        {
            FloorNumber = 9;
        }
        else if (FloorNumber > 9)
        {
            FloorNumber = 0;
        }
    }
    public void Die()
    {
        IsDead = true;
        Manager.MovingMap.ScrollSpeed = 0f;
        Manager.UiManager.BestScore.text = Manager.GetScorePlayer().ToString();
        Manager.UiManager.DeadMoment();
        Manager.ActualTime = 0f;

    }
    public void MoveLeft()
    {
        if (!IsDead)
        {
            m_Holder.transform.Rotate(36, 0, 0);
            FloorNumber++;
            UpdateFloorNumer();
        }
    }
    public void MoveRight()
    {
        if (!IsDead)
        {
            m_Holder.transform.Rotate(-36, 0, 0);
            FloorNumber--;
            UpdateFloorNumer();
        }
    }
}
