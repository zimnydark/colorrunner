﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    public Image DefaultImage;

    public Map Map;
    public MovingMap MovingMap;

    public Obstacle Obstacle;

    public GameObject Player;

    public UiManager UiManager;

    public float ObstacleSpeed = 5f;

    [SerializeField]
    private Color[] m_colors;

    [SerializeField]
    private float m_Time = 4f;

    private float m_ActualTime = 0f;
    public float ActualTime
    {
        get { return m_ActualTime; }
        set { m_ActualTime = value; }
    }
    private float m_Score = 0f;
    private float m_AllTime = 0f;

    private int m_ObjectToSpawn = 0;
    private int m_CounterOb = 0;
    public float TimeToChange 
    {
        get { return m_Time; }
        set { m_Time = value; }
    }
    private void Start()
    {
        UiManager.ImageObject.color = RandColor(Map);
        m_ActualTime = m_Time;
        m_AllTime = 0f;
    }
    private void Update()
    {
        bool checkColor = CheckActualPlayerColor();
        if (m_ActualTime<=0)
        {
            if (!checkColor)
            {
                var actualScore = GetScorePlayer();
                if (m_Score > actualScore)
                    SetScorePlayer();
                Player.GetComponent<PlayerBehaviour>().Die();
            }
            else
            {
                int rand = Random.Range(0, Map.Planes.Length);
                for(int i= m_ObjectToSpawn; i <2+ m_ObjectToSpawn; i++)
                {
                    SpawnObstacles(Map.Planes[rand].transform.position, rand);
                }
                RandomColorPlane();
                UiManager.ImageObject.color = RandColor(Map);
                m_ActualTime = TimeToChange;
            }
        }
        UiManager.Time.text = ((Mathf.Floor(m_ActualTime * 10)) / 10).ToString() + " s";
        m_ActualTime = m_ActualTime - Time.deltaTime;
        if(m_ActualTime<0)
        {
            m_ActualTime = 0f;
            m_Score = m_Score;
        }
        else
        {
            m_Score = m_Score + Time.deltaTime;
        }
        UiManager.Score.text = Mathf.Floor(((Mathf.Floor(m_Score * 10)) / 50)).ToString();
        m_AllTime = m_AllTime + Time.deltaTime;
        UpdateMapParametrs();
    }
    private void RandomColorPlane()
    {
        foreach(GameObject go in Map.Planes)
        {
            go.GetComponent<Renderer>().material.SetColor("_Color", RandColor());
        }
    }


    private Color RandColor(Map Map = null)
    {

        if (Map == null)
            return m_colors[Random.Range(0, m_colors.Length)];
        else
            return Map.Planes[Random.Range(0, 9)].GetComponent<Renderer>().material.color;
    }

    private bool  CheckActualPlayerColor()
    {
       var color = Map.Planes[PlayerBehaviour.FloorNumber].GetComponent<Renderer>().material.color;
        if (color == UiManager.ImageObject.color)
            return true;
        else
            return false;
    }

    private void SpawnObstacles(Vector3 spawn, int planeNumber)
    {

        int countMainColor = 0;
        foreach (GameObject go in Map.Planes)
        {
            if (go.GetComponent<Renderer>().material.color == UiManager.ImageObject.color)
                countMainColor++;
        }
        Color planeColor = new Color();
        if (Map.Planes[planeNumber].GetComponent<Renderer>().material.color == UiManager.ImageObject.color && countMainColor<=1)
        {
            planeColor = UiManager.ImageObject.color;
        }
        else
        {
            planeColor = m_colors[Random.Range(0, m_colors.Length)];
        }
        var gobject = Instantiate(Obstacle.Obstacles[Random.Range(0, Obstacle.Obstacles.Length)], Map.Planes[planeNumber].transform.position, Quaternion.identity);
        gobject.transform.SetParent(Map.Planes[planeNumber].transform);
        gobject.GetComponent<Renderer>().material.color = planeColor;
        gobject.transform.position = spawn;
    }


    private void SetScorePlayer()
    {
        PlayerPrefs.SetInt("Score", (int)Mathf.Floor(((Mathf.Floor(m_Score * 10)) / 50)));
    }
    private void UpdateMapParametrs()
    {   
        if (m_AllTime> 15)
        {
            MovingMap.ScrollSpeed = MovingMap.ScrollSpeed + 0.5f;
            ObstacleSpeed = ObstacleSpeed + 1f;
            m_AllTime = 0;
            m_CounterOb++;
        }
        if(m_CounterOb>3)
        {
            m_ObjectToSpawn++;
            m_CounterOb = 0;
        }
    }
    public int GetScorePlayer()
    {
        return PlayerPrefs.GetInt("Score");
    }

    public Color GetPlayersPlatformColor()
    {
        return Map.Planes[PlayerBehaviour.FloorNumber].GetComponent<Renderer>().material.color;
    }
}
