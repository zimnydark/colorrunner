﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public TextMeshProUGUI Time;
    public Image ImageObject;
    public TextMeshProUGUI Score;

    public TextMeshProUGUI BestScore;
    public Animator Dead;
    public GameObject DeadPanel;

    public AudioSys Audio;

    public void LoadLevel()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void ExtiGame()
    {
        Application.Quit();
    }
    public void DeadMoment()
    {
        Dead.SetBool("Start", true);
        DeadPanel.SetActive(true);
    }

}
